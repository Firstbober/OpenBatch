#include "../include/app.hpp"

#ifndef SYSTEM_WINDOWS
	char _getch() {
		char buf = 0;
		struct termios old = { 0 };
		fflush(stdout);
		if(tcgetattr(0, &old) < 0) {
			perror("tcsetattr()");
		}
		old.c_lflag&=~ICANON;
		old.c_lflag&=~ECHO;
		old.c_cc[VMIN]=1;
		old.c_cc[VTIME]=0;
		if(tcsetattr(0, TCSANOW, &old) < 0) {
			perror("tcsetattr ICANON");
		}
		if(read(0, &buf, 1) < 0) {
			perror("read()");
		}
		old.c_lflag|=ICANON;
		old.c_lflag|=ECHO;
		if(tcsetattr(0, TCSADRAIN, &old) < 0) {
			perror("tcsetattr ~ICANON");
		}
		return buf;
	}
#endif

App::App() {
	//
}

App::~App() {
	bat.close();
}

void App::Start(int argc, char* argv[]) {
	if(argc > 0) {
		argv0 = argv[0];
	} else {
		argv0 = "openbatch";
	}

	int s = 1;

	if(argc > 1) {
		bool c = false;
		bool k = false;
		std::string arg;

		for(int i = 1; i < argc; i++) {
			arg = argv[i];
			if(arg == "/?" || arg == "--help" || arg == "-h") {
				Help();
				exit(0);
			}
			if((arg == "/c" || arg == "-c") && !k) {
				s = s + 2;
				if(argc == s && *argv[i + 1] != '\0') {
					ParseCommand(argv[i + 1]);
				}
				if(!c) c = true;
			}
			if((arg == "/k" || arg == "-k") && !c) {
				s = s + 2;
				if(argc == s && *argv[i + 1] != '\0') {
					ParseCommand(argv[i + 1]);
				}
				if(!k) k = true;
			}
		}

		if((c || k) && s <= argc - 1) {
			StartBatFile(argv[s]);
		}

		if(c) {
			exit(0);
		}

		if(k) {
			StartPrompt();
		}
	} else {
		StartPrompt();
	}

	if(s > argc - 1) {
		StartPrompt();
	} else {
		StartBatFile(argv[s]);
	}
}

void App::StartBatFile(const std::string &filename) {
	bat.open(filename, std::ifstream::in);

	if(!bat.good()) {
		std::cerr << "Invalid Batch file" << std::endl;
		std::cout << std::endl;
		StartPrompt(false);
	}

	std::string line;

	while(std::getline(bat, line)) {
		if(line.empty() || line == "\r") continue;

		line = (line.substr(line.length() - 1) == "\r" ? line.substr(0, line.length() - 1) : line);

		if(!line.compare(0, 1, "@")) {
			line = line.substr(1);
		} else if(!disablePrompt) {
			std::cout << ParsePrompt(prompt) << line << std::endl;
		}

		ParseCommand(line);
	}
}

void App::StartPrompt(const bool &info) {
	if(info) std::cout
	<< "OpenBatch [Version 1.0]" << std::endl
	<< "(C) Copyright 2018 TheMaking" << std::endl
	<< std::endl;

	isPrompt = true;

	std::string cmd;

	while(true) {
		if(!disablePrompt) std::cout << ParsePrompt(prompt);
		std::getline(std::cin, cmd);

		if(cmd.empty()) continue;

		ParseCommand(cmd);
	}
}

std::string App::ParsePrompt(std::string str) {
	if(str.empty()) return str;

	std::size_t pos = 0;
	std::string tmp;

	while((pos = str.find("$", pos)) != std::string::npos) {
		pos++;

		tmp = str.substr(pos, 1);
		if(tmp == "A" || tmp == "a") {
			// Ampersand
			tmp = "&";
		} else if(tmp == "B" || tmp == "b") {
			// Pipe
			tmp = "|";
		} else if(tmp == "C" || tmp == "c") {
			// Left parenthesis
			tmp = "(";
		} else if(tmp == "D" || tmp == "d") {
			// Current Date (ex. Pn 2018-04-30)
			tmp = "";
		} else if(tmp == "E" || tmp == "e") {
			// Escape code (ASCII code 27)
			tmp = "\x1b";
		} else if(tmp == "F" || tmp == "f") {
			// Right parenthesis
			tmp = ")";
		} else if(tmp == "G" || tmp == "g") {
			// Greater-than sign
			tmp = ">";
		} else if(tmp == "H" || tmp == "h") {
			// Backspace (Erases previous character)
			tmp = "";
		} else if(tmp == "L" || tmp == "l") {
			// Less-than sign
			tmp = "<";
		} else if(tmp == "M" || tmp == "m") {
			// Display the remote name for Network drives
			tmp = "";
		} else if(tmp == "N" || tmp == "n") {
			// Current drive
			tmp = "";
		} else if(tmp == "P" || tmp == "p") {
			// Current drive and path
			tmp = GetCurrentDir();
		} else if(tmp == "Q" || tmp == "q") {
			// Equal sign
			tmp = "=";
		} else if(tmp == "S" || tmp == "s") {
			// Space
			tmp = " ";
		} else if(tmp == "T" || tmp == "t") {
			// Current Time (ex. 0:42:14,48)
			tmp = "";
		} else if(tmp == "V" || tmp == "v") {
			// Version number
			tmp = "1.0";
		} else if(tmp == "_") {
			// Carriage return and linefeed (CRLF)
			tmp = "\r\n";
		} else if(tmp == "$") {
			// Dollar sign
			tmp = "$";
		} else if(tmp == "+") {
			// Display plus signs (+) one for each level of the PUSHD directory stack
			tmp = "";
		}

		str = str.replace(pos - 1, 2, tmp);
	}

	return str;
}

void App::ParseCommand(const std::string &cmd) {
	std::string pcmd = ReplaceVariables(cmd);

	StrVector words = String::Split(pcmd, ' ');

	if(words[0] == "EXIT" || words[0] == "exit") {
		try {
			exit(std::find(words.begin(), words.end(), words[1]) != words.end() ? std::stoi(words[1]) : 0);
		} catch(...) {
			exit(0);
		}
	} else if(words[0] == "HELP" || words[0] == "help") {
		Cmds();
	} else if(words[0] == "CMD" || words[0] == "cmd") {
		std::string path = argv0;

		if(std::find(words.begin(), words.end(), words[1]) != words.end()) {
			path += std::string(" ") + pcmd.substr(words[0].length() + 1);
		}

		int stat = Execute(path);

		if(stat == -1) {
			std::cerr << "Unable to run '" << path << "'" << std::endl;
		} else {
			variables["errorlevel"] = stat;
		}
	} else if(words[0] == "CD" || words[0] == "cd") {
		if(std::find(words.begin(), words.end(), words[1]) != words.end()) {
			if(_chdir(pcmd.substr(words[0].length() + 1).c_str()) < 0) {
				std::cout << "Path not found." << std::endl;
			}
		} else {
			std::cout << GetCurrentDir() << std::endl;
		}
	} else if(words[0] == "ECHO" || words[0] == "echo") {
		if(std::find(words.begin(), words.end(), words[1]) != words.end()) {
			std::string text = pcmd.substr(words[0].length() + 1);
			if(text == "on" || text == "ON") {
				disablePrompt = false;
			} else if(text == "off" || text == "OFF") {
				disablePrompt = true;
			} else {
				std::cout << text << std::endl;
			}
		} else {
			std::cout << "ECHO is " << (!disablePrompt ? "off" : "on") << "." << std::endl;
		}
	} else if(!pcmd.compare(0, 5, "ECHO.") || !pcmd.compare(0, 5, "echo.")) {
		std::cout << pcmd.substr(5) << std::endl;
	} else if(words[0] == "PROMPT" || words[0] == "prompt") {
		if(std::find(words.begin(), words.end(), words[1]) != words.end()) {
			prompt = pcmd.substr(words[0].length() + 1);
		} else {
			prompt = "$P$G";
		}
	} else if(words[0] == "TITLE" || words[0] == "title") {
		if(std::find(words.begin(), words.end(), words[1]) != words.end()) {
			SetTitle(pcmd.substr(words[0].length() + 1));
		}
	} else if(words[0] == "PAUSE" || words[0] == "pause") {
		std::cout << "Press any key to continue . . . ";
		_getch();
		std::cout << std::endl << std::endl;
	} else if(words[0] == "CLS" || words[0] == "cls") {
		Clear();
	} else if(words[0] == "SET" || words[0] == "set") {
		if(std::find(words.begin(), words.end(), words[1]) != words.end()) {
			int i = 1;
			if(words[1] == "/a") i = 4;

			std::string variable = pcmd.substr(words[0].length() + i);

			if(variable.substr(0, 1) == "\"" && variable.substr(variable.length() - 1) == "\"") {
				variable = variable.substr(1);
				variable = variable.substr(0, variable.length() - 1);
			}

			if(variable.find("=") != std::string::npos) {
				StrVector temp = String::Split(variable, '=');

				if(std::find(temp.begin(), temp.end(), temp[1]) != temp.end() && !temp[1].empty()) {
					if(i == 1) {
						if(calc_variables.find(temp[0]) != calc_variables.end()) {
							calc_variables.erase(temp[0]);
						}
						variables[temp[0]] = variable.substr(temp[0].length() + 1);
					} else {
						if(variables.find(temp[0]) != variables.end()) {
							variables.erase(temp[0]);
						}
						calc_variables[temp[0]] = eval::eval(variable.substr(temp[0].length() + 1).c_str());
					}
				} else {
					if(variables.find(temp[0]) != variables.end()) {
						variables.erase(temp[0]);
					}

					if(calc_variables.find(temp[0]) != calc_variables.end()) {
						calc_variables.erase(temp[0]);
					}
				}
			} else {
				if(variables.find(variable) != variables.end()) {
					std::cout << variable << "=" << variables[variable] << std::endl;
				} else if(calc_variables.find(variable) != calc_variables.end()) {
					std::cout << variable << "=" << calc_variables[variable] << std::endl;
				} else {
					std::cout << "Environment variable " << variable << " not defined" << std::endl;
				}
			}
		} else {
			for(const auto &i: variables) {
				std::cout << i.first << "=" << i.second << std::endl;
			}

			for(const auto &i: calc_variables) {
				std::cout << i.first << "=" << i.second << std::endl;
			}
		}
	} else {
		std::string ext = String::Split(pcmd, '.').back();
		if(ext == "bat" || ext == "cmd") {
			StartBatFile(words[0]);
		} else {
			std::string path = GetProgramPath(words[0]);
			if(!path.empty()) {
				if(std::find(words.begin(), words.end(), words[1]) != words.end()) {
					path += std::string(" ") + pcmd.substr(words[0].length() + 1);
				}

				int stat = Execute(path);

				if(stat == -1) {
					std::cerr << "Unable to run '" << path << "'" << std::endl;
				} else {
					variables["errorlevel"] = std::to_string(stat);
				}
			} else {
				std::cerr << "'" << words[0] << "' is not recognized as an internal or external command, " << std::endl << "operable program or batch file." << std::endl;
			}
		}
	}

	if(!disablePrompt) std::cout << std::endl;
}

std::string App::GetProgramPath(const std::string &program) {
	std::string name = "";

	if(Exists(program) && IsFile(program)) {
		#ifdef SYSTEM_WINDOWS
			TCHAR path[255];
			GetFullPathName(TEXT(program.c_str()), 255, path, NULL);
			name = path;
		#else
			char path[255];
			realpath(program.c_str(), path);
			name = path;
		#endif
	} else {
		#ifdef SYSTEM_WINDOWS
			char delim = ';';
			char slash = '\\';
		#else
			char delim = ':';
			char slash = '/';
		#endif

		StrVector path = String::Split(std::getenv("PATH"), delim);

		for(std::string &i: path) {
			if(Exists(i + slash + program)) {
				name = i + slash + program;
				break;
			}
		}
	}

	return name;
}

bool App::Exists(const std::string &filename) {
	std::ifstream file(filename);
	return file.good();
}

bool App::IsFile(const std::string &path) {
	#ifdef SYSTEM_WINDOWS
		return !PathIsDirectory(TEXT(path.c_str()));
	#else
		struct stat s;
		if(stat(path.c_str(), &s) == 0) {
			if(s.st_mode & S_IFDIR) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	#endif
}

int App::Execute(const std::string &program) {
	FILE* handle = _popen(program.c_str(), "r");

	if(handle == NULL) {
		return false;
	}

	char buffer[128];
	while(fgets(buffer, sizeof(buffer), handle)) {  
		std::cout << buffer;  
	}

	int stat = _pclose(handle);

	return (stat < 256 ? stat : stat / 256);
}

std::string App::ReplaceVariables(std::string str) {
	if(str.empty()) return str;

	std::size_t pos = 0;
	int tmp = -1;
	std::string name;

	while((pos = str.find("%", pos)) != std::string::npos) {
		pos++;

		if(tmp == -1) {
			tmp = pos;
		} else {
			name = str.substr(tmp, pos - tmp - 1);

			if(variables.find(name) != variables.end()) {
				str = str.replace(tmp - 1, pos - tmp + 1, variables[name]);
			} else if(calc_variables.find(name) != calc_variables.end()) {
				str = str.replace(tmp - 1, pos - tmp + 1, std::to_string(calc_variables[name]));
			}

			tmp = -1;
		}
	}

	return str;
}

std::string App::GetCurrentDir() {
	char buffer[FILENAME_MAX];
	_getcwd(buffer, FILENAME_MAX);
	return std::string(buffer);
}

void App::SetTitle(const std::string &new_title) {
	title = new_title;
	#ifdef SYSTEM_WINDOWS
		SetConsoleTitle(TEXT(title.c_str()));
	#else
		char esc_start[] = { 0x1b, ']', '0', ';', 0 };
		char esc_end[] = { 0x07, 0 };
		std::cout << esc_start << title << esc_end;
	#endif
}

void App::Clear() {
	#ifdef SYSTEM_WINDOWS
		COORD topLeft = { 0, 0 };
		HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO screen;
		DWORD written;

		GetConsoleScreenBufferInfo(console, &screen);
		FillConsoleOutputCharacterA(console, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written);
		FillConsoleOutputAttribute(console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE, screen.dwSize.X * screen.dwSize.Y, topLeft, &written);
		SetConsoleCursorPosition(console, topLeft);
	#else
		std::cout << "\x1B[2J\x1B[H";
	#endif
}

void App::Cmds() {
	std::cout
	<< "For more information on a specific command, type HELP command-name" << std::endl
	/* << "ASSOC          Displays or modifies file extension associations." << std::endl
	<< "ATTRIB         Displays or changes file attributes." << std::endl
	<< "BREAK          Sets or clears extended CTRL+C checking." << std::endl
	<< "BCDEDIT        Sets properties in boot database to control boot loading." << std::endl
	<< "CACLS          Displays or modifies access control lists (ACLs) of files." << std::endl
	<< "CALL           Calls one batch program from another." << std::endl */
	<< "CD             Displays the name of or changes the current directory." << std::endl
	/* << "CHCP           Displays or sets the active code page number." << std::endl
	<< "CHDIR          Displays the name of or changes the current directory." << std::endl
	<< "CHKDSK         Checks a disk and displays a status report." << std::endl
	<< "CHKNTFS        Displays or modifies the checking of disk at boot time." << std::endl */
	<< "CLS            Clears the screen." << std::endl
	<< "CMD            Starts a new instance of the Windows command interpreter." << std::endl
	/* << "COLOR          Sets the default console foreground and background colors." << std::endl
	<< "COMP           Compares the contents of two files or sets of files." << std::endl
	<< "COMPACT        Displays or alters the compression of files on NTFS partitions." << std::endl
	<< "CONVERT        Converts FAT volumes to NTFS.  You cannot convert the" << std::endl
	<< "               current drive." << std::endl
	<< "COPY           Copies one or more files to another location." << std::endl
	<< "DATE           Displays or sets the date." << std::endl
	<< "DEL            Deletes one or more files." << std::endl
	<< "DIR            Displays a list of files and subdirectories in a directory." << std::endl
	<< "DISKPART       Displays or configures Disk Partition properties." << std::endl
	<< "DOSKEY         Edits command lines, recalls Windows commands, and" << std::endl
	<< "               creates macros." << std::endl
	<< "DRIVERQUERY    Displays current device driver status and properties." << std::endl */
	<< "ECHO           Displays messages, or turns command echoing on or off." << std::endl
	/* << "ENDLOCAL       Ends localization of environment changes in a batch file." << std::endl
	<< "ERASE          Deletes one or more files." << std::endl */
	<< "EXIT           Quits the CMD.EXE program (command interpreter)." << std::endl
	/* << "FC             Compares two files or sets of files, and displays the" << std::endl
	<< "               differences between them." << std::endl
	<< "FIND           Searches for a text string in a file or files." << std::endl
	<< "FINDSTR        Searches for strings in files." << std::endl
	<< "FOR            Runs a specified command for each file in a set of files." << std::endl
	<< "FORMAT         Formats a disk for use with Windows." << std::endl
	<< "FSUTIL         Displays or configures the file system properties." << std::endl
	<< "FTYPE          Displays or modifies file types used in file extension" << std::endl
	<< "               associations." << std::endl
	<< "GOTO           Directs the Windows command interpreter to a labeled line in" << std::endl
	<< "               a batch program." << std::endl
	<< "GPRESULT       Displays Group Policy information for machine or user." << std::endl
	<< "GRAFTABL       Enables Windows to display an extended character set in" << std::endl
	<< "               graphics mode." << std::endl */
	<< "HELP           Provides Help information for Windows commands." << std::endl
	/* << "ICACLS         Display, modify, backup, or restore ACLs for files and" << std::endl
	<< "               directories." << std::endl
	<< "IF             Performs conditional processing in batch programs." << std::endl
	<< "LABEL          Creates, changes, or deletes the volume label of a disk." << std::endl
	<< "MD             Creates a directory." << std::endl
	<< "MKDIR          Creates a directory." << std::endl
	<< "MKLINK         Creates Symbolic Links and Hard Links" << std::endl
	<< "MODE           Configures a system device." << std::endl
	<< "MORE           Displays output one screen at a time." << std::endl
	<< "MOVE           Moves one or more files from one directory to another" << std::endl
	<< "               directory." << std::endl
	<< "OPENFILES      Displays files opened by remote users for a file share." << std::endl
	<< "PATH           Displays or sets a search path for executable files." << std::endl */
	<< "PAUSE          Suspends processing of a batch file and displays a message." << std::endl
	/* << "POPD           Restores the previous value of the current directory saved by" << std::endl
	<< "               PUSHD." << std::endl
	<< "PRINT          Prints a text file." << std::endl */
	<< "PROMPT         Changes the Windows command prompt." << std::endl
	/* << "PUSHD          Saves the current directory then changes it." << std::endl
	<< "RD             Removes a directory." << std::endl
	<< "RECOVER        Recovers readable information from a bad or defective disk." << std::endl
	<< "REM            Records comments (remarks) in batch files or CONFIG.SYS." << std::endl
	<< "REN            Renames a file or files." << std::endl
	<< "RENAME         Renames a file or files." << std::endl
	<< "REPLACE        Replaces files." << std::endl
	<< "RMDIR          Removes a directory." << std::endl
	<< "ROBOCOPY       Advanced utility to copy files and directory trees" << std::endl */
	<< "SET            Displays, sets, or removes Windows environment variables." << std::endl
	/* << "SETLOCAL       Begins localization of environment changes in a batch file." << std::endl
	<< "SC             Displays or configures services (background processes)." << std::endl
	<< "SCHTASKS       Schedules commands and programs to run on a computer." << std::endl
	<< "SHIFT          Shifts the position of replaceable parameters in batch files." << std::endl
	<< "SHUTDOWN       Allows proper local or remote shutdown of machine." << std::endl
	<< "SORT           Sorts input." << std::endl
	<< "START          Starts a separate window to run a specified program or command." << std::endl
	<< "SUBST          Associates a path with a drive letter." << std::endl
	<< "SYSTEMINFO     Displays machine specific properties and configuration." << std::endl
	<< "TASKLIST       Displays all currently running tasks including services." << std::endl
	<< "TASKKILL       Kill or stop a running process or application." << std::endl
	<< "TIME           Displays or sets the system time." << std::endl */
	<< "TITLE          Sets the window title for a CMD.EXE session." << std::endl
	/* << "TREE           Graphically displays the directory structure of a drive or" << std::endl
	<< "               path." << std::endl
	<< "TYPE           Displays the contents of a text file." << std::endl
	<< "VER            Displays the Windows version." << std::endl
	<< "VERIFY         Tells Windows whether to verify that your files are written" << std::endl
	<< "               correctly to a disk." << std::endl
	<< "VOL            Displays a disk volume label and serial number." << std::endl
	<< "XCOPY          Copies files and directory trees." << std::endl
	<< "WMIC           Displays WMI information inside interactive command shell." << std::endl */
	<< std::endl
	<< "For more information on tools see the command-line reference in the online help." << std::endl;
}

void App::Help() {
	#ifdef SYSTEM_WINDOWS
		char slash = '\\';
	#else
		char slash = '/';
	#endif

	std::string appname = String::Split(argv0, slash).back();

	std::cout
	<< "Usage: " << appname << " [options...] [filename]" << std::endl
	// << "    " << appname << " [options] <filename> [args...]" << std::endl
	// << "    " << appname << " [/c <command>] [filename] [args...]" << std::endl
	// << "    " << appname << " [/k <command>] [filename] [args...]" << std::endl
	<< std::endl
	<< std::endl
	<< "    " << "/?" << "              " << "Shows this message" << std::endl
	<< "    " << "/c <command>" << "    " << "Executes command and then stops" << std::endl
	<< "    " << "/k <command>" << "    " << "Executes command and continues" << std::endl
	<< std::endl << std::endl;
}