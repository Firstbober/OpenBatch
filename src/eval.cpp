#include "../include/eval.hpp"

char eval::peek() {
	return *expression;
}

char eval::get() {
	return *expression++;
}

int eval::eval(const char* expr);

int eval::number() {
	int result = get() - '0';
	while(peek() >= '0' && peek() <= '9') {
		result = 10 * result + get() - '0';
	}
	return result;
}

int eval::factor() {
	if(peek() >= '0' && peek() <= '9') {
		return number();
	} else if(peek() == '(') {
		get(); // '('
		int result = eval(expression);
		get(); // ')'
		return result;
	} else if(peek() == '-') {
		get();
		return -factor();
	}
	return 0; // error
}

int eval::term() {
	int result = factor();
	while(peek() == '*' || peek() == '/') {
		if(get() == '*') {
			result *= factor();
		} else {
			result /= factor();
		}
	}
	return result;
}

int eval::eval(const char* expr) {
	expression = expr;

	int result = term();
	while(peek() == '+' || peek() == '-') {
		if(get() == '+') {
			result += term();
		} else {
			result -= term();
		}
	}
	return result;
}