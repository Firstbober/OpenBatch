#include "../include/app.hpp"

int main(int argc, char* argv[]) {
    auto& app = App::Instance();
    app.Start(argc, argv);
    return 0;
}