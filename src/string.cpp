#include "../include/string.hpp"

StrVector String::OneSplit(const std::string& string, char delim) {
	StrVector words = String::Split(string, delim);
	if(words.size() <= 2) {
		return words;
	}
    words.clear();
    return words;
}

StrVector String::Split(const std::string &string, char delim) {
    std::stringstream stream(string);
    std::string word;
    std::vector<std::string> words;
    while(std::getline(stream, word, delim)) {
        words.push_back(word);
    }
    return words;
}