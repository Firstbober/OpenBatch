#include <string>
#include <sstream>
#include <vector>
#include <iterator>

typedef std::vector<std::string> StrVector;

namespace String {
	StrVector OneSplit(const std::string& string, char delim); // Return 2 words
	StrVector Split(const std::string &string, char delim); // Split
}