#ifndef EVAL_HPP
#define EVAL_HPP

namespace eval {
	static const char* expression;

	char peek();
	char get();

	int eval(const char* expr);

	int number();
	int factor();
	int term();

	int eval(const char* expr);
}

#endif