#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <map>

#include "../include/config.hpp"

#ifdef SYSTEM_WINDOWS
	#include <windows.h>
	#include <conio.h>
#else
	#include <unistd.h>
	#include <termios.h>
	#include <sys/stat.h>

	#define _popen popen
	#define _pclose pclose
	#define _getcwd getcwd
	#define _chdir chdir

	char _getch();
#endif

#include "../include/string.hpp"
#include "../include/eval.hpp"

#ifndef APP_HPP
#define APP_HPP

class App final {
public:
	~App();

	// Delete operator and constructor
	App(const App &)            = delete;
	void operator=(const App &) = delete;

	// Create static function for get instance app
	static App& Instance() {
		static App instance;
		return instance;
	}

	// Functions
	void Start(int argc, char* argv[]);
	void StartBatFile(const std::string &filename);
	void StartPrompt(const bool &info = true);
	std::string ParsePrompt(std::string str);
	void ParseCommand(const std::string &cmd);
	std::string GetProgramPath(const std::string &program);
	bool Exists(const std::string &filename);
	bool IsFile(const std::string &path);
	int Execute(const std::string &program);
	std::string ReplaceVariables(std::string str);
	std::string GetCurrentDir();
	void SetTitle(const std::string &new_title);
	void Clear();
	void Cmds();
	void Help();
private:
	// Variables
	std::map<std::string, std::string> variables;
	std::map<std::string, int> calc_variables;
	std::string title;
	std::ifstream bat;
	bool isPrompt = false;
	bool disablePrompt = false;
	std::string prompt = "$P$G";
	std::string argv0;

	App();
};

#endif