# OpenBatch

OpenBatch is a fast Batch implementation for Linux, but it's also designed for Windows

## Compilation
### Linux
* **Set up a build environment**
	* Debian
	```
	# apt update && apt upgrade
	# apt install build-essential git wget unzip
	```
	* Arch
	```
	# pacman -Syu
	# pacman -S base-devel git wget unzip
	```
* **Get the required files** (Select one of these methods)
	* Clone this repository
	```
	$ git clone https://github.com/Alanovsky/OpenBatch.git
	```
	* Download ZIP of this repository
	```
	$ wget https://github.com/Alanovsky/OpenBatch/archive/master.zip && unzip master.zip
	```
* **Compile and run**
	* If you cloned this repository, `WORK_DIR` is `OpenBatch`, if you downloaded ZIP of this repository, `WORD_DIR` is `OpenBatch-master`
	```
	$ cd WORD_DIR
	$ cmake -Bbuild -H. [-G Ninja] && cmake --build build
	$ ./build/OpenBatch
	```
### Windows
* **Set up a build environment**
	* Download & install Git from https://git-scm.com/download/win
	* Right-click on desktop and click "Git Bash Here"
* **Get the required files** (Select one of these methods)
	* Clone this repository
	```
	$ git clone https://github.com/Alanovsky/OpenBatch.git
	```
	* Download ZIP of this repository: https://github.com/Alanovsky/OpenBatch/archive/master.zip
	<br>I recommend Bandizip for unpacking archives, but you can also use other methods
* **Compile and run**
	* If you cloned this repository, `WORK_DIR` is `OpenBatch`, if you downloaded ZIP of this repository, `WORD_DIR` is `OpenBatch-master` or something
	```
	$ cd WORD_DIR
	$ cmake -Bbuild -H. [-G Ninja] && cmake --build build
	$ ./build/OpenBatch.exe
	```
